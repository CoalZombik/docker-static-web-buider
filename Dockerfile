FROM node:alpine
ARG SASSC_VERSION=3.6.1

# wget -O - -q "https://api.github.com/repos/sass/sassc/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'

# SASSC
RUN apk add --no-cache libstdc++ \
	&& apk add --no-cache --virtual sassc-dependencies \
		build-base \
	&& echo "Download ${SASSC_VERSION}" \
	&& wget "https://github.com/sass/libsass/archive/${SASSC_VERSION}.zip" \
	&& unzip "${SASSC_VERSION}.zip" \
	&& rm "${SASSC_VERSION}.zip" \
	&& mv "libsass-${SASSC_VERSION}" "libsass" \
	&& wget "https://github.com/sass/sassc/archive/${SASSC_VERSION}.zip" \
	&& unzip "${SASSC_VERSION}.zip" \
	&& rm "${SASSC_VERSION}.zip" \
	&& mv "sassc-${SASSC_VERSION}" "sassc" \
	&& SASSC_PATH=$(pwd)/sassc SASS_LIBSASS_PATH=$(pwd)/libsass make -C sassc -j6 \
	&& cp sassc/bin/sassc /usr/bin/sassc \
	&& rm -r sassc libsass \
	&& apk del sassc-dependencies

# Minifier
RUN npm install html-minifier -g \
	&& npm install cssnano-cli -g \
	&& npm install uglify-js -g \
	&& npm install svgo -g \
	&& npm install nunjucks-cli -g