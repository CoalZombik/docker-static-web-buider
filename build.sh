#!/bin/sh

# Get latest version (https://gist.github.com/lukechilds/a83e1d7127b78fef38c2914c4ececc3c)
SASSC_VERSION=$(wget -O - -q "https://api.github.com/repos/sass/sassc/releases/latest" | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/')
echo "Latest sassc version: $SASSC_VERSION"

SASSC_MAJOR=`echo $SASSC_VERSION | cut -d. -f1`
SASSC_MINOR=`echo $SASSC_VERSION | cut -d. -f2`

# Check docker image existing
set +e
wget -O /dev/null "https://index.docker.io/v1/repositories/$CI_REGISTRY_IMAGE/tags/$SASSC_VERSION" > /dev/null 2>&1
ret=$?
set -e
if [ $ret -eq 0 ]; then
	echo "Docker image existing, build skipped"
else # Build
	echo "Building"
	docker build --pull -t "$CI_REGISTRY_IMAGE:$SASSC_VERSION" --build-arg SASSC_VERSION=$SASSC_VERSION .
    docker push "$CI_REGISTRY_IMAGE:$SASSC_VERSION"
    
    # Push minor version
    docker tag "$CI_REGISTRY_IMAGE:$SASSC_VERSION" "$CI_REGISTRY_IMAGE:$SASSC_MAJOR.$SASSC_MINOR"
    docker push "$CI_REGISTRY_IMAGE:$SASSC_MAJOR.$SASSC_MINOR"
    # Push major version
    docker tag "$CI_REGISTRY_IMAGE:$SASSC_VERSION" "$CI_REGISTRY_IMAGE:$SASSC_MAJOR"
    docker push "$CI_REGISTRY_IMAGE:$SASSC_MAJOR"
    # Push latest
    docker tag "$CI_REGISTRY_IMAGE:$SASSC_VERSION" "$CI_REGISTRY_IMAGE:latest"
    docker push "$CI_REGISTRY_IMAGE:latest"
fi
